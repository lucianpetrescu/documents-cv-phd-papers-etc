##CONTENTS

This is an empty repository, which contains only this file (Readme.md), with links to all  documents and videos in my Google Drive. **Videos** of some of my work and teaching can be found at the bottom of this file.

###Website

Most of my work is structured on my website [https://sites.google.com/site/lucianalexpetrescu/](https://sites.google.com/site/lucianalexpetrescu/). The website links back to this Bitbucket account.

###CV

- [Curriculum Vitae](https://drive.google.com/open?id=0B3yTES0pEsWXRmNJR1NWaVBJZzg) I find this concise format better: only 2 pages, better structure and also more aesthetically pleasing.
- [Curriculum Vitae - EUROPASS](https://drive.google.com/open?id=0B3yTES0pEsWXbU8zRll5STZJZ1k) 

###PHD

[Thesis - Rendering massive 3D scenes in real-time](https://drive.google.com/open?id=0B3yTES0pEsWXbHI1UXgxWXNEZWs). I developed sparse algorithms for real-time rendering : 

- an object based imperfect voxelization which can be used as an acceleration structure for real-time global illumination  
- a sparse tridimensional ray-guiding map, which guides rays/paths/vpls to high energy areas.  
- an algorithm which can select anything drawable in rasterization rendering, in optimal time and space complexity.  
- a framework for the analysis of deferred rendering algorithms  
- other small contributions 

###Open Source

[https://bitbucket.org/lucianpetrescu/](https://bitbucket.org/lucianpetrescu/). For properly documented free libraries with zero external dependencies check the [public](https://bitbucket.org/lucianpetrescu/public) section. Just plug and play. It contains:
- an OpenGL window manager built over GLFW with lots of ease of use functionality and no configuration needed.  
- debugging tools (stack trace, allocation and leak checker)  
- a fast performance logger  
- an all purpose GPU timer  
- an OpenGL monitoring tool (in early stages but can already find bugs)  
- an image loading library  
- font generation tools  

Also in the [base](https://bitbucket.org/lucianpetrescu/) link less documented free source code repositories:  
- gpu generalized Canny edge detection    
- linearized GPU blur  
- gpu spinlocks  
- an example implementation of the rasterized GPU picking algorithm  
- an example implementation of the soft sparse connected component algorithm  
- a teaching repository  
- repository with complete implementation of the A-Buffer per-pixel linked-list Order Independent Transparency algorithm
- an example implementation of the sparse pseudorandom normals algorithm


###Papers and Tech Reports

- [GPU Sparse Ray-Traced Segmentation](https://ieeexplore.ieee.org/document/8718269) (K-dimensional sparse segmentation algorithm that uses ray tracing to sample&label space)
- [Virtual Deferred Rendering](https://drive.google.com/open?id=0B3yTES0pEsWXeW9kUHRod2liMUk) (rendering technique which uses virtual texturing to minimize bandwidth)
- [Analyzing Deferred Rendering Techniques](https://drive.google.com/open?id=0B3yTES0pEsWXbUhldmFnZ1NlMEk) (analyzes 20+ deferred rendering rasterization rendering techniques)
- [Efficient Picking Through Atomic Operations](https://drive.google.com/open?id=0B3yTES0pEsWXZk9vZ1F5VERaMDQ) (presentation)
- [Efficient Picking Through Atomic Operations](https://drive.google.com/open?id=0B3yTES0pEsWXMS1uZUNBTDJBMEU) (paper) (selection/picking transient geometry in rasterization rendering)
- [A GPU task generator for rendering](https://drive.google.com/open?id=0B3yTES0pEsWXUE04S01aWGZQdms) (task generation during hardware rasterization)
- [GPGPU Based Non-photorealistic Rendering of Volume Data](https://drive.google.com/open?id=0B3yTES0pEsWXYTZzQ05NRFR5NGc) (secondary author, enhanced perception)
- [Guarded Order Independent Transparency](https://drive.google.com/open?id=0B3yTES0pEsWXRi01MmVac0JjQ2M) (order independent transparency in rasterization)
- [Real time reconstruction of volumes from  very large datasets using CUDA](https://drive.google.com/open?id=0B3yTES0pEsWXcXhoZmMtU0lZbTA) (tech report on cuda marching cubes)
- [Real time stairs detection on mobile devices](https://drive.google.com/open?id=0B3yTES0pEsWXLUtPVGp4LVBvaW8) (secondary author, stairs detection)
- [Presentation - A short intuitive state of the art of rendering](https://drive.google.com/open?id=0B3yTES0pEsWXUnJtV09wVDcxbDg) (200+ slides of top down view on rendering in general, intuition heavy, barely any math, discusses the key insights behind algorithm development and rendering as sorting. I have been told that this is a very useful document, therefore I have made it public)

## WORK VIDEOS

Unfortunately video embedding is not supported in the Markdown syntax. Here I use a Markdown hack which shows a screenshot from each video and then links to the video on my Google Drive, but going there is inevitable. Viewing videos on Google Drive might be at very low quality, it might be better to download and open with a local player. Almost all videos are at 720p resolution and have hardcoded subtitles.


####Sparse Pseudo-Random Sampled Normals
NOTE: pending publication
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXa1l6Z1UyMzZldWM)](https://drive.google.com/open?id=0B3yTES0pEsWXb0FzNVE2WUluZlE "link to video"). From the [SoundOfVision](https://soundofvision.net/) project. This algorithm uses pseudo-random sampling and stochastic rejection to compute normals from depth. It uses few samples and it produces better results then state of the art averaging methods, because this sampling pattern minimizes depth artifact propagation and transforms structured noise into random noise, which is much easier to filter then the former. 

####GPU Sparse Ray-Traced Segmentation

[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXRmNxdld6bGdHdUE)](https://drive.google.com/open?id=0B3yTES0pEsWXUExoeFpJYk9xWTg "link to video"). From the [SoundOfVision](https://soundofvision.net/) project. GPU Sparse Ray-Traced Segmentation computes n-dimensional labeling using the flux of the input (e.g. normalized gradient for images), sampling the input with rays in order to achieve extremely fast coverage of the search space. The algorithm uses lockless atomic instruction based GPU synchronization in order to prevent costly pipeline stalls. After the sparse coverage stage, optional GPU stages can be used to fully label the image and perform region growing. 

In this video the algorithm is used on a video, where it quickly partitions each frame into potential segments. It runs in ~10ms on a consumer laptop, in this case streaming back the result is costlier then the algorithm. While the streaming method can easily be accelerated, it was not implemented for the pipeline with which this video was made, as any type of frame delay was not compatible with other concurrently running algorithms.

Here is another video which shows how this algorithm is used as an input for best free space detection and for scene labeling.
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXZ19KbkNzOWwwYlk)](https://drive.google.com/open?id=0B3yTES0pEsWXZlRROHZXNmY2UVk "link to video").

####Sparse Sampled Best Free Space
NOTE: pending publication

[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXS082THF4SkpuRlk)](https://drive.google.com/open?id=0B3yTES0pEsWXWTl1dTRNUFBJMlk "link to video"). From the [SoundOfVision](https://soundofvision.net/) project. The algorithm finds the best advance space in an indoor environment, based on camera depth. The green/red boxes at the top of the video show the safe/unsafe status of a person standing/walking at the camera positions (ground not found, ground unreachable, close to objects, etc). Runs in just a couple of ms.
 
####Labeled Point Cloud Visualization

[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXMG9rYzZvbG5hXzg)](https://drive.google.com/open?id=0B3yTES0pEsWXZnBjMHlUZ0ZDNFE "link to video"). From the [SoundOfVision](https://soundofvision.net/) project. A visualization of a labeled point cloud. Note that the input can be **very** noisy, as the input video has a low depth resolution.


####3D for Medicine
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXS2NVcEpoRTBGcVE)](https://drive.google.com/open?id=0B3yTES0pEsWXUTVteXhHUWs4OGM "link to video") I wrote this application for the [SABIMAS(romanian)](http://se.cs.pub.ro/SABIMAS/) research project. The application reconstructs geometry from very large datasets (GBs) of medical data in DICOM format with a variant of the Marching Cubes algorithm,  in real-time, using CUDA. The application was successfully tested by health care professionals.

####Advanced Picking
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXMFNBNjk0cVRlTms)](https://drive.google.com/open?id=0B3yTES0pEsWXaVNsVXYweEt3RkE "link to video"). This video accompanied the "Efficient Picking Through Atomic Operations" paper. It shows that the introduced picking algorithm can select any type of geometry with minimal computational and memory resource usage. It can select alpha culled geometry (the leaves), transient geometry (the tessellated ground triangles exist only on the gpu), particle systems (the smoke). It also takes opacity into account (selects everything that is visible on that pixel, sometimes 3-4 objects).
Red = selected object. Green = selected primitive. Blue = selected sub-primitive (generated by shader(s) in each frame).

####Order Independent Transparency - Rendering
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXWWRDcHFKZjEyX28)](https://drive.google.com/open?id=0B3yTES0pEsWXX29wM2xrYnFuVUE "link to video"). This is a video of the linked-list per pixel implementation of Order Independent Transparency (OIT). As transparency is an order dependent operation, the effect can't be achieved correctly without implicit (approximation/stochastic functions) or explicit (sorting) order. 

####Order Independent Transparency - Vision
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXR0NlY0Iya1hUeWs)](https://drive.google.com/open?id=0B3yTES0pEsWXNzJpQW5HM3BYS1U "link to video"). The same algorithm as in the previous entry, but this one was done for a vision presentation, showcasing the industrial applications of the algorithm. 

 
 
####Raymarched depth inpainting

[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXQmNuYm9CaW1kaG8)](https://drive.google.com/open?id=0B3yTES0pEsWXd0RPVEs4LXV6STg "link to video"). From the [SoundOfVision](https://soundofvision.net/) project. Raymarching is used to inpaint (filter-in) the black (unsampled) entries in the depth map.
 
##Teaching videos##

These videos showcase some topics from labs that I've taught (there are many others..)


####Deferred Rendering
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXNHpRQjZpZGxJdWc)](https://drive.google.com/open?id=0B3yTES0pEsWXcEcwWm82YnUyNnM "link to video"). The classic low complexity rasterization rendering algorithm for direct illumination without shadows.

####Global Illumination with Reflective Shadow Maps and Virtual Point Lights
NOTE: the output is exaggerated in order to clearly showcase properties. 
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXTWVjODM4cEd1R28)](https://drive.google.com/open?id=0B3yTES0pEsWXSHJPZzlwdks2Rm8 "link to video"). Global Illumination with a simple implementation of Virtual Point Lights. A Reflective Shadow Map is generated from the perspective of the dominant light, storing color, position and normals. Virtual Point Lights are generated by sampling the Reflective Shadow Map. Their positions are obtained by displacing the positions of the samples on the directions of the normals. The colors of the virtual point lights are given by the colors of the samples. The energies are obtained using the angles between the dominant light and the sample normals.

####Hardware tessellation and Displacement Mapping
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXNFQ4bENVam5QWGc)](https://drive.google.com/open?id=0B3yTES0pEsWXd1ZBd0c3c3NqWE0 "link to video"). Hardware Tesellation creates transient geometry during rasterization rendering. Displacement mapping is used to alter the transient geometry. This process creates detailed meshes from low-poly input.

####Direct Illumination
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXUXB2TjFyWVlHbjA)](https://drive.google.com/open?id=0B3yTES0pEsWXSlFKQzNoUjhRZlU "link to video"). An example of the base lab, loading 3D models, shading them, illuminating them with point, spot and directional lights, etc.

####Post Processing
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXNW1EeFdNNm01QUE)](https://drive.google.com/open?id=0B3yTES0pEsWXOU9UaEs2RHd0UGM "link to video"). GPU post processing: edge detection, gaussian blur, depth linearization.

####Shadow Mapping
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXM1JINWliWTdjeUk)](https://drive.google.com/open?id=0B3yTES0pEsWXUzZWZlBfc1RiM3c "link to video"). The classic shadow mapping algorithm with all its positives and negatives.

####Surface Effects
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXZ1JvbXpzQ3pCMGM)](https://drive.google.com/open?id=0B3yTES0pEsWXd3cwcVRkaXZQelU "link to video"). Image space algorithms which need just a single quad (couple of triangles) to convincingly simulate micro-geometry. Bump mapping, iterative parallax occlusion mapping, relaxed cone stepping.

